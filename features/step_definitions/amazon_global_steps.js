var seleniumWebdriver = require('selenium-webdriver');
var {defineSupportCode} = require('cucumber');

defineSupportCode(function({Given, When, Then}) {

    Given('I go to the {string} product', function (productId) {
        // Write code here that turns the phrase above into concrete actions
        this.driver.get("http://www.amazon.com/dp/" + productId);
    });
});