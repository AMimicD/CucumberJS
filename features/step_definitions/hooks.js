var {defineSupportCode} = require('cucumber');

defineSupportCode(function({Before, After}) {
    After(function() {
        return this.driver.quit();
    });
});