var seleniumWebdriver = require('selenium-webdriver');
var {defineSupportCode} = require('cucumber');

defineSupportCode(function({Given, When, Then}) {

    Given('I am on {string}', function (url) {
        // Write code here that turns the phrase above into concrete actions
        this.driver.get(url);
    });
});